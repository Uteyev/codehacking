@extends('layouts/admin')

@section('content')

    <h1>Category Edit</h1>

    <div class="col-sm-6">
        {!! Form::model(
                $category,
                [
                    'method'	=> 'PATCH',
                    'action'	=> array('AdminCategoriesController@update', $category->id),
                ]) !!}

        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <br>

        <div class="row">
            <div class="col-sm-1">
                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}
            </div>

            <div class="col-sm-1"></div>
            <div class="col-sm-1">

                {!! Form::open([
                    'method'	=> 'DELETE',
                    'action'	=> ['AdminCategoriesController@destroy', $category->id],
                ]) !!}



                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

                {!! Form::close() !!}
            </div>
            <div class="col-sm-9"></div>
        </div>
    </div>
    <div class="col-sm-6">

    </div>
@stop