@extends('layouts.admin')

@section('content')
    <h1>Edit Post</h1>

    @include( 'includes/form_error')
    {!! Form::model(
            $post,
            [
                'method'	=> 'PATCH',
                'action'	=> array('AdminPostsController@update', $post->id),
                'files'     => true,
            ])
    !!}

    <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
    <br>
    <div class="form-group">
        {!! Form::label('category_id', 'Category:') !!}
        {!! Form::select('category_id', array('' => 'Choose Category...') + $categories, null, ['class' => 'form-control']) !!}
    </div>
    <br>
    <div class="form-group">
        {!! Form::label('photo_id', 'Post Thumbnail:') !!}
        {!! Form::file('photo_id', null, ['class' => 'form-control']) !!}
    </div>
    <br>
    <div class="form-group">
        {!! Form::label('body', 'Description:') !!}
        {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
    </div>
    <br>

    <div class="row">
        <div class="col-sm-1">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>
        <div class="col-sm-1">
            {!! Form::open([
                'method'	=> 'DELETE',
                'action'	=> ['AdminPostsController@destroy', $post->id],
            ]) !!}

            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

            {!! Form::close() !!}
        </div>
        <div class="col-sm-10"></div>
    </div>


    {!! Form::close() !!}
@stop