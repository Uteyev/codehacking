@extends('layouts/admin')

@section('content')
    <h1>Edit User</h1>
    <div class="row">
        @include( 'includes/form_error')
    </div>

    <div class="row">
        <div class="col-sm-3">
            <img src="{{$user->photo ? $user->photo->file : 'http://placehold.it/400x400'}}" class="img-responsive img-rounded">
        </div>
        <div class="col-sm-9">
            {!! Form::model(
                $user,
                [
                    'method'	=> 'PATCH',
                    'action'	=> array('AdminUsersController@update', $user->id),
                    'files'     => true,
                ]) !!}

            <div class="form-group">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::email('email', null, ['class' => 'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
                {!! Form::label('role_id', 'Role:') !!}
                {!! Form::select('role_id', $roles, null, ['class' => 'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
                {!! Form::label('is_active', 'Status:') !!}
                {!! Form::select('is_active', array(0 => 'Not Active', 1 => 'Active'), null, ['class' => 'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
                {!! Form::label('photo_id', 'User Picture:') !!}
                {!! Form::file('photo_id', null, ['class' => 'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
                {!! Form::label('password', 'Password:') !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
            </div>
            <br>

            <div class="row">
                <div class="col-sm-1">
                    {!! Form::submit('Update User', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>

                <div class="col-sm-1"></div>
                <div class="col-sm-1">

                    {!! Form::open([
                        'method'	=> 'DELETE',
                        'action'	=> ['AdminUsersController@destroy', $user->id],
                    ]) !!}



                    {!! Form::submit('Delete User', ['class' => 'btn btn-danger']) !!}

                    {!! Form::close() !!}
                </div>
                <div class="col-sm-9"></div>
            </div>
        </div>
    </div>
@stop