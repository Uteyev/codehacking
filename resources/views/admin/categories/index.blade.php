@extends('layouts/admin')

@section('content')

    @if( session('category_deleted') )
        <p class="bg-danger">{{session('category_deleted')}}</p>
    @endif

    <h1>Categories</h1>

    <div class="col-sm-6">
        {!! Form::open([
            'method'	=> 'POST',
            'action'	=> 'AdminCategoriesController@store',
        ]) !!}

        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <br>
        {!! Form::submit('Add Category', ['class' => 'btn btn-primary']) !!}

        {!! Form::close() !!}
    </div>
    <div class="col-sm-6">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Created</th>
                <th>Updated</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td><a href="{{route('admin.categories.edit', $category->id)}}">{{$category->name}}</a></td>
                    <td>{{$category->created_at->diffForHumans()}}</td>
                    <td>{{$category->updated_at->diffForHumans()}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop